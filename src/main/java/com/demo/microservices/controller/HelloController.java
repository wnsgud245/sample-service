package com.demo.microservices.controller;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.microservices.model.Hello;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
public class HelloController {
	private String msgTemplate = "%s 님 반갑습니다.";
	private final AtomicLong visitorCounter = new AtomicLong();
	
	@ApiOperation(value = "Hello Api입니다.")
	@ApiImplicitParams({
			@ApiImplicitParam(name="name", value="이름", required = true, dataType = "String", paramType = "query", defaultValue = "홍길동")
	})
	@GetMapping("/hello")
	public Hello getHelloMsg(@RequestParam(value = "name") String name) {
		return new Hello(visitorCounter.incrementAndGet(), String.format(msgTemplate, name));
	}
}
