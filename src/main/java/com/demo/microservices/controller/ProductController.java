package com.demo.microservices.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Log4j2
public class ProductController {

    @Value("${garage.product.server}")
    String productServer;

    @Value("${garage.product.port}")
    String productPort;

    @ApiOperation(value = "Product 전체를 불러오는 API 입니다")
    @GetMapping("/products")
    public ResponseEntity<?> getProducts() {
        String url = String.format("http://%s:%s/products", productServer, productPort);
        Object products;
        try {
            RestTemplate restTemplate = new RestTemplate();
            products = restTemplate.getForObject(url, Object.class);
        } catch (Exception e) {
            log.error("ERROR", e);
            throw new RuntimeException(e);
        }

        log.info("success get products");
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
