package com.demo.microservices.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.microservices.dao.SampleUserDao;
import com.demo.microservices.model.SampleUser;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class UserController {

    private final SampleUserDao sampleUserDao;

    public UserController(SampleUserDao sampleUserDao) {
        this.sampleUserDao = sampleUserDao;
    }
    @ApiOperation(value = "유저 전체 조회 API")
    @GetMapping("/users")
    public ResponseEntity<?> getUserAll() {
        List<SampleUser> list;
        try {
            log.info("Start DB select");
            list = sampleUserDao.selectUserAll();
        } catch (Exception e) {
            log.error("ERROR", e);
            throw new RuntimeException(e);
        }
        log.info("user counts : " + list.size());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @ApiOperation(value = "특정 유저 조회 API")
    @GetMapping("/users/{userId}")
    public ResponseEntity<?> getUser(@PathVariable String userId) {
        SampleUser user;
        try {
            log.info("Start DB select");
            user = sampleUserDao.selectUser(userId);
        } catch (Exception e) {
            log.error("ERROR", e);
            throw new RuntimeException(e);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @ApiOperation(value = "유저 생성 API")
    @PostMapping("/users")
    public ResponseEntity<?> insertUser(@RequestBody SampleUser user) {
        int result;
        try {
            log.info("Start DB select");
            result = sampleUserDao.insertUser(user);
        } catch (Exception e) {
            log.error("ERROR", e);
            throw new RuntimeException(e);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "유저 정보 수정 API")
    @PutMapping("/users")
    public ResponseEntity<?> updateUser(@RequestBody SampleUser user) {
        int result;
        try {
            log.info("Start DB select");
            result = sampleUserDao.updateUser(user);
        } catch (Exception e) {
            log.error("ERROR", e);
            throw new RuntimeException(e);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "유저 삭제 API")
    @DeleteMapping("/users/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable String userId) {
        int result;
        try {
            log.info("Start DB select");
            result = sampleUserDao.deleteUser(userId);
        } catch (Exception e) {
            log.error("ERROR", e);
            throw new RuntimeException(e);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
